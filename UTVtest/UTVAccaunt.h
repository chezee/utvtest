//
//  UTVAccaunt.h
//  UTVtest
//
//  Created by Илья Пупкин on 12/14/16.
//  Copyright © 2016 Ilya Gorevoy. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface UTVAccaunt : NSObject <NSCoding>

@property (strong, nonatomic) NSString *username;
@property (strong, nonatomic) NSString *password;
@property (strong, nonatomic) NSString *token;

@end
