//
//  UTVTableViewCell.m
//  UTVtest
//
//  Created by Илья Пупкин on 12/11/16.
//  Copyright © 2016 Ilya Gorevoy. All rights reserved.
//

#import "UTVTableViewCell.h"

@implementation UTVTableViewCell {
    NSInteger height;
}


- (void) awakeFromNib{
    [super awakeFromNib];
}

- (void) configureWithItem:(NSDictionary *)item {
    self.contentView.contentMode = UIViewContentModeScaleAspectFit;
    height = [[NSString stringWithFormat:@"%@", [item objectForKey:@"height"]] integerValue];
    [_image setFrame: CGRectMake(0, 2, _cellView.frame.size.width, height)];
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        NSString *string = [NSString stringWithFormat:@"%@", [item objectForKey:@"thumbnail_url"]];
        NSURL *thumbnailURL = [NSURL URLWithString:[[string componentsSeparatedByString:@"?"] objectAtIndex:0]];
        NSData *data = [NSData dataWithContentsOfURL:thumbnailURL];
        UIImage *thumbnailImage = [UIImage imageWithData:data];
        _image.image = thumbnailImage;
    });
    [_title setAdjustsFontSizeToFitWidth:YES];
    NSString *subtractTitle = [NSString stringWithFormat:@"%@", [item objectForKey:@"title"]];
    if([subtractTitle length] >= 20) {
        subtractTitle = [subtractTitle stringByReplacingCharactersInRange:NSMakeRange(19, [subtractTitle length] - 19) withString:@"..."];
    }
    [_title setFrame:CGRectMake(8, height + 8, 64, 20)];
    [_likes setFrame:CGRectMake(_title.frame.size.width + 8, height + 8, 24, 20)];
    _title.text = [NSString stringWithFormat:@"%@", subtractTitle];
    _likes.text = [NSString stringWithFormat:@"have %@ likes", [[item objectForKey:@"likes_count"] stringValue]];    
}

- (AVPlayer *) playVideo:(NSDictionary *)item {
        self.playerViewController.player = [AVPlayer playerWithURL:[NSURL URLWithString:[item objectForKey:@"complete_url"]]];
        self.playerViewController.view.backgroundColor = [UIColor whiteColor];
        [self.playerViewController.view setFrame:CGRectMake(0, 2, _cellView.frame.size.width, height)];
        [self.playerViewController setVideoGravity:@"AVLayerVideoGravityResize"];
        [_cellView addSubview:self.playerViewController.view];
    return self.playerViewController.player;
}


@end
