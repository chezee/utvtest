//
//  UTVTableViewCell.h
//  UTVtest
//
//  Created by Илья Пупкин on 12/11/16.
//  Copyright © 2016 Ilya Gorevoy. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AVFoundation/AVFoundation.h>
#import <AVKit/AVKit.h>


@interface UTVTableViewCell : UITableViewCell

@property (strong, nonatomic) IBOutlet UIView *cellView;
@property (strong, nonatomic) IBOutlet UIImageView *image;
@property (weak, nonatomic) IBOutlet UILabel *title;
@property (weak, nonatomic) IBOutlet UILabel *likes;
@property (weak, atomic) NSMutableArray *videoQueue;
@property (weak, nonatomic) AVPlayerViewController *playerViewController;

- (void) configureWithItem:(NSDictionary *)item;
- (AVPlayer *) playVideo:(NSDictionary *)item;

@end
