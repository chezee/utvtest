//
//  UTVLoginViewController.h
//  UTVtest
//
//  Created by Илья Пупкин on 12/22/16.
//  Copyright © 2016 Ilya Gorevoy. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UTVAccaunt.h"

@interface UTVLoginViewController : UIViewController

@property (strong, nonatomic) UTVAccaunt *accaunt;

@end
