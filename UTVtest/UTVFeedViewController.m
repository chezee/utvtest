//
//  SecondViewController.m
//  UTVtest
//
//  Created by Илья Пупкин on 12/11/16.
//  Copyright © 2016 Ilya Gorevoy. All rights reserved.
//

#import "UTVFeedViewController.h"
#import "UTVTableViewCell.h"
#import "UTVLoginViewController.h"

static NSString *cellIdentifier = @"newCell";
static NSString *accauntKey = @"UTVAccauntKey";

@interface UTVFeedViewController ()

@end

@implementation UTVFeedViewController {
    NSArray *fetchedVids;
    UIRefreshControl *refresh;
    UTVTableViewCell *configCell;
    NSString *recvToken;
}


- (void)viewDidLoad {
    [super viewDidLoad];
   
    [self loadUserAccaunt];
    if(!self.accaunt.token) {
        [self.navigationController presentViewController:[UTVLoginViewController new] animated:NO completion:nil];
    }
    else {
        [self fetch];
    }
    
    UIButton *logoutButton = [[UIButton alloc] initWithFrame:CGRectMake(0,
                                                                        self.view.frame.size.height-90,
                                                                        self.view.bounds.size.width,
                                                                        40)];
    [logoutButton setAutoresizingMask:UIViewAutoresizingFlexibleBottomMargin];
    [logoutButton setBackgroundColor:[UIColor darkGrayColor]];
    [logoutButton setTitleColor:[UIColor redColor] forState:UIControlStateNormal];
    [logoutButton setTitle:@"Logout" forState:UIControlStateNormal];
    [logoutButton addTarget:self action:@selector(logoutFromAcc) forControlEvents:UIControlEventTouchDown];
    [self.view addSubview:logoutButton];
    
    [_table registerClass:[UTVTableViewCell class] forCellReuseIdentifier:cellIdentifier];
    [_table registerNib:[UINib nibWithNibName:@"UTVTableViewCell" bundle:nil] forCellReuseIdentifier:cellIdentifier];
    
    refresh = [[UIRefreshControl alloc] init];
    [_table addSubview:refresh];
    [refresh addTarget:self action:@selector(refreshTable) forControlEvents:UIControlEventValueChanged];
    
}

- (void) refreshTable {
    [refresh beginRefreshing];
    fetchedVids = NULL;
    [self fetch];
    [_table reloadData];
    [refresh endRefreshing];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return [fetchedVids count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    configCell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier
                                                 forIndexPath:indexPath];
    [configCell configureWithItem:[fetchedVids objectAtIndex:indexPath.row]];
    
    return configCell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(nonnull NSIndexPath *)indexPath {
    CGFloat height = 0.f;
    height += 30 + [[[fetchedVids objectAtIndex:indexPath.row] objectForKey:@"height"] floatValue];
    return height;
}

- (void) tableView:(UITableView *)tableView willDisplayCell:(nonnull UTVTableViewCell *)cell forRowAtIndexPath:(nonnull NSIndexPath *)indexPath {
    //[cell playVideo:[fetchedVids objectAtIndex:indexPath.row]];
    //[cell.playerViewController.player play];
    
}

-(void) tableView:(UITableView *)tableView didEndDisplayingCell:(UTVTableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    [cell.playerViewController.player pause];
}

- (void)loadUserAccaunt {
    NSData *userAccaunt = [[NSUserDefaults standardUserDefaults] objectForKey:accauntKey];
    if(userAccaunt)
        self.accaunt = [NSKeyedUnarchiver unarchiveObjectWithData:userAccaunt];
}

- (void)saveUserAccaunt {
    NSData *userAccaunt = [NSKeyedArchiver archivedDataWithRootObject:self.accaunt];
    [[NSUserDefaults standardUserDefaults] setObject:userAccaunt forKey:accauntKey];
}

- (void)fetch {
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"https://api.vid.me/videos/following?access_token=%@", self.accaunt.token]];
    NSError *error;
    NSString *json = [[NSString alloc] initWithContentsOfURL:url
                                                    encoding:NSUTF8StringEncoding
                                                       error:&error];
    NSData *data = [json dataUsingEncoding:NSUTF8StringEncoding];
    NSDictionary *dict = [NSJSONSerialization JSONObjectWithData:data options:0 error:&error];
    fetchedVids = [dict objectForKey:@"videos"];
}

- (void)logoutFromAcc {
    fetchedVids = nil;
    self.accaunt = nil;
    [self saveUserAccaunt];
    [self.navigationController presentViewController:[UTVLoginViewController new] animated:YES completion:nil];
}
@end
