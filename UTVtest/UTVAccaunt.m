//
//  UTVAccaunt.m
//  UTVtest
//
//  Created by Илья Пупкин on 12/14/16.
//  Copyright © 2016 Ilya Gorevoy. All rights reserved.
//

#import "UTVAccaunt.h"

static NSString *usernameKey = @"UTVUsernameKey";
static NSString *passwordKey = @"UTVPasswordKey";
static NSString *tokenKey = @"UTVTokenKey";

@implementation UTVAccaunt

- (id) initWithCoder:(NSCoder *)coder {
    self = [super init];
    
    self.username = [coder decodeObjectForKey:usernameKey];
    self.password = [coder decodeObjectForKey:passwordKey];
    self.token = [coder decodeObjectForKey:tokenKey];
    
    return  self;
}

- (void) encodeWithCoder:(NSCoder *) coder {
    [coder encodeObject:self.username forKey:usernameKey];
    [coder encodeObject:self.password forKey:passwordKey];
    [coder encodeObject:self.token forKey:tokenKey];
}

@end
