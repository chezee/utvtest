//
//  UTVLoginViewController.m
//  UTVtest
//
//  Created by Илья Пупкин on 12/22/16.
//  Copyright © 2016 Ilya Gorevoy. All rights reserved.
//

#import "UTVLoginViewController.h"

static NSString *accauntKey = @"UTVAccauntKey";
int loginValue = 0;

@interface UTVLoginViewController ()

@end

@implementation UTVLoginViewController {
    NSString *recvToken;
    UITextField *username;
    UITextField *password;
}


- (void)viewDidLoad {
    [super viewDidLoad];
    [self.view setBackgroundColor:[UIColor whiteColor]];
    UIButton *loginButton = [UIButton new];
    
    [loginButton addTarget:self action:@selector(loginToService) forControlEvents:UIControlEventTouchDown];
    [loginButton setTitle:@"Log in" forState:UIControlStateNormal];
    loginButton.backgroundColor = [UIColor redColor];
    loginButton.frame = CGRectMake(self.view.center.x - 50,
                                     self.view.center.y + 50,
                                     100,
                                     24);
    [self.view addSubview:loginButton];
    
    username = [[UITextField alloc] initWithFrame:CGRectMake(self.view.center.x - 50,
                                                             self.view.center.y - 20,
                                                             150,
                                                             24)];
    username.backgroundColor = [UIColor grayColor];
    username.text = @"Login";
    [self.view addSubview:username];
    
    password = [[UITextField alloc] initWithFrame:CGRectMake(self.view.center.x - 50,
                                                             self.view.center.y + 20,
                                                             150,
                                                             24)];
    password.backgroundColor = [UIColor grayColor];
    password.text = @"Password";
    [self.view addSubview:password];
    
    self.accaunt = [UTVAccaunt new];
}

- (void)saveUserAccaunt {
    NSData *userAccaunt = [NSKeyedArchiver archivedDataWithRootObject:self.accaunt];
    [[NSUserDefaults standardUserDefaults] setObject:userAccaunt forKey:accauntKey];
}

- (void)loadUserAccaunt {
    NSData *userAccaunt = [[NSUserDefaults standardUserDefaults] objectForKey:accauntKey];
    if(userAccaunt)
        self.accaunt = [NSKeyedUnarchiver unarchiveObjectWithData:userAccaunt];
}


- (BOOL)isValidUsername:(NSString *)checkString
{
    if([checkString length]> 3)
        return YES;
    else
        loginValue = 2;
    return NO;
}

- (BOOL)isValidPassword:(NSString *)passwordString
{
    if([passwordString length]>4)
        return YES;
    else
        loginValue = 1;
    return NO;
}



- (void)loginToService {
    if([self isValidUsername:username.text])
        self.accaunt.username = username.text;
    if([self isValidPassword:password.text])
        self.accaunt.password = password.text;
    switch (loginValue) {
        case 0:
            [self.view endEditing:YES];
            [self sendLoginRequestWithEmail:self.accaunt.username
                                andPassword:self.accaunt.password];
            [self saveUserAccaunt];
            break;
        case 1:
            NSLog(@"Password is invalid");
            break;
        case 2:
            NSLog(@"Email is invalid");
            break;
    }
}

- (void)sendLoginRequestWithEmail:(NSString *)email andPassword:(NSString *)password {
    
    NSURLSessionConfiguration *config = [NSURLSessionConfiguration defaultSessionConfiguration];
    NSURLSession *session = [NSURLSession sessionWithConfiguration:config
                                                          delegate:nil
                                                     delegateQueue:[NSOperationQueue mainQueue]];
    NSURL *loginURL = [NSURL URLWithString:@"https://api.vid.me/auth/create"];
    NSMutableURLRequest *URLRequest= [NSMutableURLRequest requestWithURL:loginURL];
    NSString *params = [NSString stringWithFormat:@"username=%@&password=%@", self.accaunt.username, self.accaunt.password];
    NSLog(@"%@", params);
    [URLRequest setHTTPMethod:@"POST"];
    [URLRequest setHTTPBody:[params dataUsingEncoding:NSUTF8StringEncoding]];
    
    NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:URLRequest
                                                completionHandler:^(NSData *data, NSURLResponse *response, NSError *error)
                                      {
                                          NSDictionary *responseDict = [NSJSONSerialization JSONObjectWithData:data
                                                                                                       options:NSJSONReadingAllowFragments
                                                                                                         error:nil];
                                          if([responseDict objectForKey:@"status"] == 0 || [[responseDict objectForKey:@"status"] isEqualToString:@"false"])
                                              NSLog(@"%@", [responseDict objectForKey:@"error"]);
                                          else {
                                              recvToken = [[responseDict objectForKey:@"auth"] objectForKey:@"token"];
                                              self.accaunt.token = recvToken;
                                              NSLog(@"accaunt token: %@", self.accaunt.token);
                                              [self.view endEditing:YES];
                                              [self saveUserAccaunt];
                                              [self dismissViewControllerAnimated:YES completion:nil];
                                          }
                                      }];
    [dataTask resume];
}

@end

