//
//  FirstViewController.h
//  UTVtest
//
//  Created by Илья Пупкин on 12/11/16.
//  Copyright © 2016 Ilya Gorevoy. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UTVTableViewCell.h"

@interface UTVFeaturedViewController : UITableViewController <UITableViewDelegate, UITableViewDataSource>

@property (strong, nonatomic) IBOutlet UITableView *table;

@end

