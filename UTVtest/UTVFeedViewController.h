//
//  SecondViewController.h
//  UTVtest
//
//  Created by Илья Пупкин on 12/11/16.
//  Copyright © 2016 Ilya Gorevoy. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UTVAccaunt.h"

@interface UTVFeedViewController : UIViewController <UITableViewDelegate, UITableViewDataSource>

@property (weak, nonatomic) IBOutlet UITableView *table;
@property (strong, nonatomic) UTVAccaunt *accaunt;

@end

