//
//  FirstViewController.m
//  UTVtest
//
//  Created by Илья Пупкин on 12/11/16.
//  Copyright © 2016 Ilya Gorevoy. All rights reserved.
//

#import "UTVFeaturedViewController.h"
#import "UTVTableViewCell.h"

static NSString *cellIdentifier = @"featuredCell";

@interface UTVFeaturedViewController ()

@end

@implementation UTVFeaturedViewController {
    NSArray *fetchedVids;
    UIRefreshControl *refresh;
    UTVTableViewCell *protCell;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [_table registerClass:[UTVTableViewCell class] forCellReuseIdentifier:cellIdentifier];
    [_table registerNib:[UINib nibWithNibName:@"UTVTableViewCell" bundle:nil] forCellReuseIdentifier:cellIdentifier];
    [self fetch];

    refresh = [[UIRefreshControl alloc] init];
    [_table addSubview:refresh];
    [refresh addTarget:self action:@selector(refreshTable) forControlEvents:UIControlEventValueChanged];
}

- (void) refreshTable {
    [refresh beginRefreshing];
    [self fetch];
    [_table reloadData];
    [refresh endRefreshing];
}
#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return [fetchedVids count];
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    protCell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier
                                               forIndexPath:indexPath];
    [protCell configureWithItem:[fetchedVids objectAtIndex:indexPath.row]];
    
    return protCell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(nonnull NSIndexPath *)indexPath
{
    CGFloat height = 0.f;
    height += 30 + [[[fetchedVids objectAtIndex:indexPath.row] objectForKey:@"height"] floatValue];
    return height;
}

- (void) tableView:(UITableView *)tableView willDisplayCell:(nonnull UTVTableViewCell *)cell forRowAtIndexPath:(nonnull NSIndexPath *)indexPath {
    //[cell playVideo:[fetchedVids objectAtIndex:indexPath.row]];
    //[cell.playerViewController.player play];
    
}

-(void) tableView:(UITableView *)tableView didEndDisplayingCell:(UTVTableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    [cell.playerViewController.player pause];
}

- (void) fetch {
    NSURL *url = [NSURL URLWithString:@"https://api.vid.me/videos/featured"];
    NSError *error;
    NSString *json = [[NSString alloc] initWithContentsOfURL:url
                                                    encoding:NSUTF8StringEncoding
                                                       error:&error];
    NSData *data = [json dataUsingEncoding:NSUTF8StringEncoding];
    NSDictionary *dict = [NSJSONSerialization JSONObjectWithData:data options:0 error:&error];
    fetchedVids = [dict objectForKey:@"videos"];
}


@end
